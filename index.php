<!-- Header -->
<?php include ('master_header.php');?>

        <!-- Portfolio -->
        <?php include ('portfolio.php');?>

        <!-- About Section -->
        <?php include ('about.php');?>

        <!-- Contact Section -->
        <?php include ('contact.php');?>

        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
        <div class="scroll-top page-scroll visible-xs visble-sm">
            <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>

<!-- Footer -->
<?php include ('master_footer.php');?>

