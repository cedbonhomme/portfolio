<?php ?>
<section class= "success" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>About</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            Looking for an internship from April 6 to June 12.
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3><i class="fa fa-code"></i>Computer skills</h3>
                            </div>
                            <table class="table">
                                <tr>
                                    <td><b>Languages</b></td>
                                    <td>C/C++, Qt, QML, Java, C#, Bash, PowerPc Assembly</td>
                                </tr>
                                <tr>
                                    <td><b>Web</b></td>
                                    <td>HTML5/CSS, JavaScript, jQuery, Php, Ruby, Ruby On Rails, Bootstrap, JSON, XML, Wordpress CMS</td>
                                </tr>
                                <tr>
                                    <td><b>Database</b></td>
                                    <td>Mysql, SQL Server</td>
                                </tr>
                                <tr>
                                    <td><b>Platform</b></td>
                                    <td>Gnu/Linux, Android, Windows</td>
                                </tr>
                                <tr>
                                    <td><b>Softwares</b></td>
                                    <td>QtCreator, Netbeans, IntelliJ IDEA, Vim, Git, Mercurial, Visual Studio, CodeBlocks, LATEX, Office</td>
                                </tr>
                                <tr>
                                    <td><b>Graphic Library</b></td>
                                    <td>Qt, OpenGL, SFML, SDL</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3><i class="fa fa-university"></i>Education</h3>
                            </div>
                            <table class="table">
                                <tr>
                                    <td>2015–2013</td>
                                    <td><b>Preparation for a Computer Science Diploma</b></td>
                                    <td>The University of Bordeaux, France</td>
                                </tr>
                                <tr>
                                    <td>2013–2012</td>
                                    <td><b>French Cram School (CPGE)</b></td>
                                    <td>Lycee Gustave Eiffel, Bordeaux, France</td>
                                </tr>
                                <tr>
                                    <td>2012–2011</td>
                                    <td><b>A level in Science and Mathematics</b></td>
                                    <td>Lycee les Iris, Lormont, France</td>
                                </tr>
                            </table>
                        </div>


                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3><i class="fa fa-language"></i>Languages</h3>
                            </div>
                            <table class="table">
                                <tr>
                                    <td><b>English</b></td>
                                    <td>Bilingual proficiency</td>
                                </tr>
                                <tr>
                                    <td><b>French</b></td>
                                    <td>Mothertongue</td>
                                </tr>
                                <tr>
                                    <td><b>Spanish</b></td>
                                    <td>Reading, writing</td>
                                </tr>
                            </table>

                        </div>
                        <p></p>
                    </div>
                </div>
            </div>
        </section>

